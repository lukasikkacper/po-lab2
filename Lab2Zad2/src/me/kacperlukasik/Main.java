package me.kacperlukasik;

public class Main {

    public static void main(String[] args)
    {
        Rational a = new Rational(1,3);
        Rational b = new Rational(2,6);

        System.out.println(String.format("(%s) + (%s) = ",a.toString(),b.toString()) + a.add(b).toString());
        System.out.println(String.format("(%s) * (%s) = ",a.toString(),b.toString()) + a.mul(b).toString());
        System.out.println(String.format("(%s) - (%s) = ",a.toString(),b.toString()) + a.sub(b).toString());
        System.out.println(String.format("(%s) / (%s) = ",a.toString(),b.toString()) + a.div(b).toString());
        System.out.println(String.format("(%s) równa się (%s) ? ",a.toString(),b.toString()) + a.equals(b));
        System.out.println("Wynik metody compareTo to: " + a.compareTo(b));
    }
}
