package me.kacperlukasik;

public class Rational
{
    private int licznik,mianownik;

    public Rational(int licznik, int mianownik)
    {
        this.licznik = licznik;
        this.mianownik = mianownik;
    }

    public Rational add(Rational arg)
    {
        Rational a = new Rational(this.licznik,this.mianownik);
        Rational b = new Rational(arg.licznik,arg.mianownik);

        int nww = nww(a.mianownik, b.mianownik);

        a.licznik = a.licznik*(nww/a.mianownik);
        a.mianownik = nww;

        b.licznik = b.licznik*(nww/b.mianownik);
        b.mianownik = nww;

        return shorteningFraction(new Rational(a.licznik+b.licznik, a.mianownik));
    }

    public Rational mul(Rational arg)
    {
        Rational result = new Rational(this.licznik*arg.licznik,this.mianownik*arg.mianownik);

        return shorteningFraction(result);
    }

    public Rational sub(Rational arg)
    {
        Rational a = new Rational(this.licznik,this.mianownik);
        Rational b = new Rational(arg.licznik,arg.mianownik);

        int nww = nww(a.mianownik,b.mianownik);

        a.licznik = a.licznik*(nww/a.mianownik);
        a.mianownik = nww;

        b.licznik = b.licznik*(nww/b.mianownik);
        b.mianownik = nww;

        return shorteningFraction(new Rational(a.licznik-b.licznik, a.mianownik));
    }

    public Rational div(Rational arg)
    {
        return mul(inverseFraction(arg));
    }

    public boolean equals(Rational arg)
    {
        double a = (double) this.licznik/this.mianownik;
        double b = (double) arg.licznik/arg.mianownik;

        return a==b;
    }

    public int compareTo(Rational arg)
    {
        double a = (double)this.licznik/this.mianownik;
        double b = (double)arg.licznik/arg.mianownik;

        if(a==b) return 0;
        else if (a<b) return -1;

        return 1;
    }

    @Override
    public String toString()
    {
        return String.format("%d/%d",this.licznik, this.mianownik);
    }

    private int nwd(int a, int b)
    {
        if (b == 0)
            return a;

        return nwd(b, (a % b));
    }

    private int nww(int a, int b)
    {
        return a*b/nwd(a,b);
    }

    private Rational shorteningFraction(Rational arg)
    {
        if(arg.licznik == 0 || arg.mianownik == 0)
            return new Rational(0,1);

        int nwd = nwd(arg.licznik, arg.mianownik);

        return new Rational(arg.licznik/nwd,arg.mianownik/nwd);
    }

    private Rational inverseFraction(Rational arg)
    {
        return new Rational(arg.mianownik,arg.licznik);
    }
}
