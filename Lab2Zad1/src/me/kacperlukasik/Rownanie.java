package me.kacperlukasik;

import java.awt.List;
import java.util.ArrayList;

public class Rownanie
{
    private double a,b,c;

    public Rownanie(int a, int b, int c)
    {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public void setA(double a)
    {
        this.a = a;
    }

    public void setB(double b)
    {
        this.b = b;
    }

    public void setC(double c)
    {
        this.c = c;
    }

    public double obliczY(int x)
    {
        double result = a*Math.pow(x,2)+b*x+c;

        return result;
    }

    public ArrayList<Double> obliczPierwiastki()
    {
        ArrayList<Double> results = new ArrayList<>();

        double delta = Math.pow(b,2) - 4*a*c;
        double sqrtDelta = Math.sqrt(delta);

        if(delta>0)
        {
            double x1 = (-b-sqrtDelta)/(2*a);
            double x2 = (-b+sqrtDelta)/(2*a);

            results.add(x1);
            results.add(x2);
        }
        else if (delta==0)
        {
            double x1 = -b/(2*a);

            results.add(x1);
        }

        return results;
    }

    public void wypiszPierwiastki()
    {
        ArrayList<Double> pierwiastki = obliczPierwiastki();
        if(pierwiastki.size() > 0)
        {
            System.out.println("Liczba pierwiastków: " + pierwiastki.size());
            for(double pierwiastek : pierwiastki)
            {
                System.out.println("Pierwiastek: " + pierwiastek);
            }
        }
        else
        {
            System.out.println("Brak pierwiastkow !");
        }

    }
}
