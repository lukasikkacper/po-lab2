package me.kacperlukasik;

public class Main {

    public static void main(String[] args)
    {
	    Rownanie rownanie = new Rownanie(1,1,1);
	    for(int i = 2; i < 8; i++)
        {
            System.out.format("f(%d)=%f \n",i,rownanie.obliczY(i));
        }
        rownanie.wypiszPierwiastki();
    }
}
