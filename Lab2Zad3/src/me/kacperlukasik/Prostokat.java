package me.kacperlukasik;

public class Prostokat
{
    private int x;
    private int y;
    private int szerokosc;
    private int wysokosc;

    public void init(int x, int y, int szerokosc, int wysokosc)
    {
        this.x = x;
        this.y = y;
        this.szerokosc = szerokosc;
        this.wysokosc = wysokosc;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public int getSzerokosc()
    {
        return szerokosc;
    }

    public int getWysokosc()
    {
        return wysokosc;
    }

    public int obliczPole()
    {
        return szerokosc*wysokosc;
    }

    public void sprawdzPunkt(int x, int y)
    {
        if (containsIn(x,y))
            System.out.format("Punkt P(%d,%d) nalezy do prostakata !", x, y);
        else
            System.out.format("Punkt P(%d,%d) nie nalezy do prostakata !", x, y);
    }

    public void wypiszDane()
    {
        String result = "";
        result+=String.format("Wsporzedne lewego dolnego wierzchołka (x,y): (%d,%d)\n",x,y);
        result+=String.format("Wysokosc(h) = %d | Szerokosc = %d\n",wysokosc,szerokosc);
        result+=String.format("Pole prostokata: %d", obliczPole());

        System.out.println(result);
    }

    private boolean containsIn(int x, int y)
    {
        int yy = this.y + wysokosc;
        int xx = this.x + szerokosc;

        return (x>=this.x&&x<=xx) && (y>=this.y && y <= yy);
    }
}
