package me.kacperlukasik;

import java.util.Random;
import java.util.Scanner;

public class Osoba
{
    private int Id;
    private String Nazwisko;
    private int Rok;
    private int Miesiac;
    private int Dzien;

    public void readFromKeyboard()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj id: ");
        this.Id = scanner.nextInt();

        System.out.println("Podaj nazwisko: ");
        this.Nazwisko = scanner.next();

        System.out.println("Podaj rok urodzenia: ");
        this.Rok = scanner.nextInt();

        System.out.println("Podaj miesiac urodzenia: ");
        this.Miesiac = scanner.nextInt();

        System.out.println("Podaj dzien urodzenia: ");
        this.Dzien = scanner.nextInt();
    }

    public void initRandomData()
    {
        this.Id = randomInRange(1,99);
        this.Nazwisko = "naz" + randomInRange(0,99);
        this.Rok = randomInRange(1960,1984);
        this.Miesiac = randomInRange(1,12);
        this.Dzien = randomInRange(1,31);
    }

    private int randomInRange(int min, int max)
    {
        Random random = new Random();

        return random.nextInt((max-min)+1) + min;
    }

    public void printData()
    {
        System.out.println(this.toString());
    }

    @Override
    public String toString()
    {
        String result = "";

        result += "ID: " + this.Id + "\n";
        result += "Nazwisko: " + this.Nazwisko + "\n";
        result += String.format("Data urodzenia: %s-%s-%d",(this.Dzien<10 ? "0"+this.Dzien : this.Dzien), (this.Miesiac<10 ? "0"+this.Miesiac : this.Miesiac), this.Rok);

        return result;
    }

    public static int writeRandomDataToTab(Osoba[] osoby, int n)
    {
        Osoba osoba = new Osoba();
        osoba.initRandomData();
        osoby[n] = osoba;

        return n+1;
    }

    public static void printDataFromTab(Osoba[] osoby, int n)
    {
        for(int i = 0; i < n; i++)
        {
            System.out.println("----------");
            osoby[i].printData();
        }
    }

    public static void printDataFromTab(Osoba[] osoby, int n, String nazwisko)
    {
        for(int i = 0; i < n; i++)
        {
            if(osoby[i].Nazwisko.equalsIgnoreCase(nazwisko))
            {
                System.out.println("----------");
                osoby[i].printData();
            }
        }
    }

    public static void printDataFromTab(Osoba[] osoby, int n, int minId, int maxId)
    {
        for(int i = 0; i < n; i++)
        {
            Osoba check = osoby[i];
            if(check.Id>=minId&&check.Id<=maxId)
            {
                System.out.println("----------");
                check.printData();
            }
        }
    }
}
