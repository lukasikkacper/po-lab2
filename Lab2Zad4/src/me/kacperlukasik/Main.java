package me.kacperlukasik;

public class Main {

    public static void main(String[] args) {
        Osoba o1 = new Osoba();
        Osoba o2 = new Osoba();

        o1.readFromKeyboard();
        o2.initRandomData();

        System.out.println("------------");
        o1.printData();
        System.out.println("------------");
        o2.printData();
        System.out.println("------------");
    }
}
